#! /usr/bin/env python
#-*- coding: utf-8 -*-

import pygame
import os

# path = "C:/Users\pedro/Desktop/media pygame"
# dir = os.getcwd()
dir = os.path.dirname(__file__)
filename = dir + "/media pygame"

pygame.init()
clock = pygame.time.Clock()
screen = pygame.display.set_mode((640, 480))
done = False
is_blue = True
x = 30 # posicao inicial do quadrado
y = 30



while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: # fecha a janela se carregares na cruz
            done = True


         # a partir daqui é codigo opcional





        if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE: # muda a cor do quadrado se carregares espaço
            is_blue = not is_blue

    pressed = pygame.key.get_pressed()
    if pressed[pygame.K_UP]:
        y -= 3
    if pressed[pygame.K_DOWN]:
        y += 3
    if pressed[pygame.K_LEFT]:
        x -= 3
        pygame.mixer.music.stop()
    if pressed[pygame.K_RIGHT]:
        x += 3
        pygame.mixer.music.load(os.path.join(filename, "06 - Rammstein - Buck Dich.mp3"))
        pygame.mixer.music.play(0)

    screen.fill((0,0,0))
    if is_blue:
        color = (0, 128, 255)
    else:
        color = (255, 100, 0)
    pygame.draw.rect(screen, color, pygame.Rect(x, y, 60, 60))

    # pressed = pygame.key.get_pressed()
    # if pressed[pygame.K_UP]:
    #     x -= 3
    # if pressed[pygame.K_DOWN]:
    #     x += 3
    #
    # font = pygame.font.SysFont("Verdana", x)
    # text = font.render("Benfica", True, (255, 255, 255))
    #
    # screen.fill((0, 0, 0))
    # screen.blit(text,
    #             (320 - text.get_width() // 2, 240 - text.get_height() // 2))
    # abaixo disto é codigo que da jeito estar la

    clock.tick(60) # para assumir intervalo minimo de 1/60 s
    pygame.display.flip()

