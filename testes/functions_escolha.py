#! /usr/bin/env python
# -*- coding: utf-8 -*-

import random

from random import randint


def start():
    a = 0
    equipas = list()
    global selectedTeam


    class Team(object):
        def __init__(self, name, attack, defence, teamplay):
            self.name = name
            self.attack = attack
            self.defence = defence
            self.teamplay = teamplay

    equipas.append(Team("Real Madrid", 95, 90, 95))
    equipas.append(Team("Barcelona", 92, 88, 94))
    equipas.append(Team("Benfica", 85, 79, 89))
    equipas.append(Team("FC Porto", 82, 80, 85))
    equipas.append(Team("Chelsea", 90, 88, 90))
    equipas.append(Team("Manchester United", 92, 90, 93))
    equipas.append(Team("Paris Saint-Germain", 93, 90, 90))
    equipas.append(Team("Monaco", 84, 83, 85))
    equipas.append(Team("Bayern München", 91, 85, 90))
    equipas.append(Team("BVB Dormund", 88, 85, 87))
    equipas.append(Team("Juventus", 94, 90, 89))
    equipas.append(Team("AC Milan", 86, 80, 82))
    equipas.append(Team("Ajax", 79, 77, 85))
    equipas.append(Team("Arsenal", 82, 80, 84))
    equipas.append(Team("Atlético Madrid", 82, 88, 80))
    equipas.append(Team("FC Zenit", 80, 75, 76))

    for i in range(0, len(equipas)):
        print('%d - %s' % (i + 1, equipas[i].name))

    input1 = input('Escolhe a tua equipa: ')

    selectedTeam = equipas[int(input1) - 1]

    return equipas, selectedTeam

def setMode():
    print('\n1 - Ataque\n'
          '2 - Defesa\n'
          '3 - Neutro\n')

    strategy = input('Tipo de Estratégia: ')

    if strategy == '1':
        modos = [True, False]
    elif strategy == '2':
        modos = [False, True]
    else:
        modos = [False, False]
    return modos


def doRound(contenders, modos):
    winners = list()
    for j, _ in enumerate(contenders):
        if contenders[j].name == selectedTeam.name:
            index = j

    for i, _ in enumerate(contenders):
        if i % 2 == 0:
            # matchScore = event(contenders[i], contenders[i + 1])
            plays = numberevents(contenders[i].teamplay, contenders[i + 1].teamplay)
            dif_attack = float(contenders[i].attack - contenders[i + 1].attack)

            homeplays, awayplays = getplays(plays, dif_attack)


            homefatigue = 0
            awayfatigue = 0

            homegoals, awaygoals = match(homeplays, awayplays, contenders[i], contenders[i+1], homefatigue, awayfatigue,
                                         modos)

            # homegoals, homefatigue = event(int(homeplays), contenders[i], contenders[i + 1], 0)
            # awaygoals, awayfatigue = event(int(awayplays), contenders[i + 1], contenders[i], 0)

            homeTeam = contenders[i].name
            awayTeam = contenders[i + 1].name

            if homegoals > awaygoals:
                winners.append(contenders[i])

                print('%25s %s - %s %s' % (homeTeam, str(homegoals), str(awaygoals), awayTeam))
            # guarantee that no result ends in a tie in the knockout round
            elif homegoals < awaygoals:
                winners.append(contenders[i + 1])

                print('%25s %s - %s %s' % (homeTeam, str(homegoals), str(awaygoals), awayTeam))
            else:
                extratime_homegoals, extratime_awaygoals = match(homeplays/3, awayplays/3, contenders[i], contenders[i + 1], homefatigue, awayfatigue,
                                                                 modos)
                # extratime_awaygoals, fatigue = event(plays/3, contenders[i + 1], contenders[i], awayfatigue)
                if extratime_homegoals > extratime_awaygoals:
                    winners.append(contenders[i])
                    print('%25s %s - %s %s (%s-%s after extra time)' % (
                    homeTeam, homegoals, awaygoals, awayTeam, homegoals + extratime_homegoals,
                    awaygoals + extratime_awaygoals))
                elif extratime_awaygoals > extratime_homegoals:
                    winners.append(contenders[i + 1])
                    print('%25s %s - %s %s (%s-%s after extra time)' % (
                    homeTeam, homegoals, awaygoals, awayTeam, homegoals + extratime_homegoals,
                    awaygoals + extratime_awaygoals))
                else:
                    homepenalties, awaypenalties = penalties()
                    print('%25s %s - %s %s (%s-%s after extra time) (%s-%s in penalties)' % (
                        homeTeam, homegoals, awaygoals, awayTeam, homegoals + extratime_homegoals,
                        awaygoals + extratime_awaygoals, homepenalties, awaypenalties))
                    if homepenalties > awaypenalties:
                        winners.append(contenders[i])
                    else:
                        winners.append(contenders[i+1])



                #print(homeTeam + " " + str(homegoals) + " vs " +  + " " + str(awaygoals) + awayTeam)

    return winners, index


def getplays(plays, dif):
    if dif == 0:
        homeplays = plays / 2
        awayplays = plays - homeplays
    elif dif > 0:
        homeplays = round(plays * ((0.45 / 98) * dif + 0.5))
        awayplays = abs(plays - homeplays)
    elif dif < 0:
        awayplays = round(plays * ((0.45 / 98) * abs(dif) + 0.5))
        homeplays = abs(plays - awayplays)

    return homeplays, awayplays


def event(goals, teamatt, teamdefen, fatigue, modos):

    defenseStat = teamdefen.defence
    basechance = 0.08

    if teamatt.name == selectedTeam.name:
        if modos[0] == True:
            basechance = 0.10
        elif modos[1] == True:
            basechance = 0.06
        else:
            basechance = 0.08

    elif teamdefen.name == selectedTeam.name:
        if modos[0] == True:
            defenseStat = teamdefen.defence - 10
        elif modos[1] == True:
            defenseStat = teamdefen.defence + 10



    r = random.random()
    goalchance = basechance + (((teamatt.attack - defenseStat) / 2) * 0.002) - (fatigue * 0.002)
    if r < goalchance:
        goals += 1
    fatigue += 0.5
    return goals, fatigue


def match(hplays, aplays, contendershome, contendersaway, homefatigue, awayfatigue, modos):
    homeplaysdone = 0
    awayplaysdone = 0
    homegoals = 0
    awaygoals = 0
    for k in range(0, int(hplays + aplays)):
        r = random.random()
        if (r > hplays / (hplays + aplays) and homeplaysdone < hplays) or awayplaysdone == aplays:
            homegoals, homefatigue = event(homegoals, contendershome, contendersaway, homefatigue, modos)
            homeplaysdone += 1
        elif (r <= hplays / (int(hplays + aplays)) and awayplaysdone < aplays) or homeplaysdone == hplays:
            awaygoals, awayfatigue = event(awaygoals, contendersaway, contendershome, awayfatigue, modos)
            awayplaysdone += 1
    return homegoals, awaygoals


def penalties():
    homepenalties = 0
    awaypenalties = 0
    for i in range(0, 5):
        homepenalty = penalty(),
        awaypenalty = penalty(),
        if homepenalty is True:
            homepenalties += 1
        if awaypenalty is True:
            awaypenalties += 1
    if homepenalties == awaypenalties:
        while homepenalties == awaypenalties:
            homepenalty = penalty()
            if homepenalty is True:
                homepenalties += 1
            awaypenalty = penalty()
            if awaypenalty is True:
                awaypenalties += 1
    return homepenalties, awaypenalties


def penalty():
    scored = None
    r = random.random()
    goalchance = 0.8
    if r < goalchance:
        scored = True
    return scored

# def event(numplays, teamatt, teamdefen, fatigue):
#     goals = 0
#     attack = dict()
#     chance = dict()
#     goalscorer = list()
#     for i in range(0, numplays):
#         r = random.random()
#         goalchance = 0.08 + (((teamatt.attack - teamdefen.defence) / 2) * 0.002) - (fatigue * 0.002)
#         if r < goalchance:
#             goals += 1
#
#         fatigue += 0.5
#     return goals, fatigue


def numberevents(hometeamplay, awayteamplay):
    baseplays = 24
    plays = baseplays + (hometeamplay + awayteamplay - 100) * baseplays / 100
    return plays