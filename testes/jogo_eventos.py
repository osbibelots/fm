#! /usr/bin/env python
#-*- coding: utf-8 -*-

import random
from random import randint

teams = list()
minutes_event = list()
a = 0
winners = list()

goals = 0



class Team(object):
    def __init__(self, name, attack, defence, teamplay):
        self.name = name
        self.attack = attack
        self.defence = defence
        self.teamplay = teamplay

teams.append(Team("Real Madrid", 95, 90, 95))
teams.append(Team("Benfica", 89, 81, 90))


def numeroeventos(hometeamplay, awayteamplay):
    baseplays = 24
    plays = baseplays + (hometeamplay + awayteamplay - 100) * baseplays/100
    return plays

plays = numeroeventos(teams[0].teamplay, teams[1].teamplay)
dif_attack = float(teams[0].attack - teams[1].attack)

while a < 100:
    if dif_attack == 0:
        homeplays = j/2
        awayplays = j - homeplays
    elif dif_attack > 0:
        # homeplays = round(j / ((4 / dif_attack) + 1))
        homeplays = round(j * ((0.45/98) * dif_attack + 0.5))
        awayplays = j - homeplays
    elif dif_attack < 0:
        # awayplays = round(j / ((4 / abs(dif_attack)) + 1))
        awayplays = round(j * ((0.45/98) * abs(dif_attack) + 0.5))
        homeplays = j - awayplays

    def event(numplays, att, defen):
        goals = 0
        for i in range(0, numplays):
            r = random.random()
            goalchance = 0.08 + (((att-defen)/2) * 0.002)
            if r < goalchance:
                goals += 1
        return goals

    homegoals = event(int(homeplays), teams[0].attack, teams[1].defence)
    awaygoals = event(int(awayplays), teams[1].attack, teams[0].defence)

    if homegoals > awaygoals:
        winners.append(teams[0].name)
    elif homegoals < awaygoals:
        winners.append(teams[1].name)

    a += 1

    if a >= 1 and a <= 10:
        print("Real Madrid " + str(homegoals) + " vs " + str(awaygoals) + " Benfica")

for i, _ in enumerate(teams):
    print(teams[i].name + " ganhou " + str(winners.count(teams[i].name)) + " vezes!")



# for i in range(1, 18):
#     minutes_event.append(i*5)

