import abc
import pygame
import math
import GameManager


class SceneBase:
    def __init__(self):
        self.next = self

    @abc.abstractmethod
    def processInput(self, events, pressed_keys):
        print("uh-oh, you didn't override this in the child class")

    @abc.abstractmethod
    def update(self):
        print("uh-oh, you didn't override this in the child class")

    @abc.abstractmethod
    def render(self, screen):
        print("uh-oh, you didn't override this in the child class")

    def switchToScene(self, next_scene):
        self.next = next_scene

    def terminate(self):
        self.switchToScene(None)


class MenuScene(SceneBase):
    _buttonWidthRatio = 0.5
    _buttonHeightRatio = 0.2

    button = pygame.Rect(0, 0, 0, 0)
    buttonText = pygame.Rect(0, 0, 0, 0)

    def update(self):
        pass

    def render(self, screen):
        screen.fill((0, 0, 0))
        self.drawMenu(screen)

    def processInput(self, events, pressed_keys):
        for event in events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                mousePos = pygame.mouse.get_pos()

                if self.button.collidepoint(mousePos):
                    print("cliquei hue")

    def drawMenu(self, screen):

        buttonX = (GameManager.getGameWidth() * self._buttonWidthRatio) - \
                  (GameManager.getGameWidth() * math.pow(self._buttonWidthRatio, 2))
        buttonY = (GameManager.getGameHeight() * self._buttonHeightRatio) - \
                  (GameManager.getGameHeight() * math.pow(self._buttonHeightRatio, 2))
        self.button.topleft = (buttonX, buttonY)
        self.button.width = GameManager.getGameWidth() * self._buttonWidthRatio
        self.button.height = GameManager.getGameHeight() * self._buttonHeightRatio

        text = GameManager.font.render("Test button", True, (255, 255, 255))
        buttonTextX = buttonX + self.button.width / 2 - text.get_width() / 2
        buttonTextY = buttonY + self.button.height / 2 - text.get_height() / 2
        self.buttonText.topleft = (buttonTextX, buttonTextY)

        pygame.draw.rect(screen, (0, 0, 120), self.button)
        screen.blit(text, self.buttonText)


GameManager.run_game(MenuScene())
