#! /usr/bin/env python
# -*- coding: utf-8 -*-


import random
from random import randint
from knockout_round_draw import getplays
from knockout_round_draw import numberevents
from knockout_round_draw import match
from knockout_round_draw import event
from operator import attrgetter
import xlrd
import os
import numpy as np

equipas = list()
playerlist = list()


class Team(object):
    points = 0
    wins = 0
    draws = 0
    losses = 0

    def __init__(self, name, teamplay, playerlist = None, attack = None, defence = None):
        self.name = name
        if playerlist is None:
            self.playerlist = []
        else:
            self.playerlist = playerlist
        if attack is None:
            self.attack = 0
        if defence is None:
            self.defence = 0
        self.teamplay = teamplay

    def adds(self, member):
        self.playerlist.append(member)
        # print('Added %s to the team!') % member.name


class Player(object):
    def __init__(self, name, position, attack, defence):
        self.name = name
        self.position = position
        self.attack = attack
        self.defence = defence

pypath = os.path.dirname(os.path.realpath(__file__))
file = 'players_teams.xlsx'
fname = os.path.join(pypath, file)

# Open the workbook
xl_workbook = xlrd.open_workbook(fname)

xl_sheet = xl_workbook.sheet_by_index(0)

row = xl_sheet.row(1)  # 1st row

for i in range(0, 20):
    if (i != 3 and (i) % 5 == 0) or i == 0:
        t = Team(row[i].value, teamplay=row[i+1].value)
        equipas.append(t)
        att = list()
        defe = list()
        for j in range(3, 14):
            rowplayer = xl_sheet.row(j)
            t.adds(Player(rowplayer[i].value, rowplayer[i + 1].value, rowplayer[i + 2].value, rowplayer[i + 3].value))
            if rowplayer[i + 2].value != 0:
                att.append(int(rowplayer[i + 2].value))
            if rowplayer[i + 3].value != 0:
                defe.append(int(rowplayer[i + 3].value))
        t.attack = sum(att)/float(len(att))
        t.defence =sum(defe)/float(len(defe))




print('lol')




equipas.append(Team("Chelsea", 90, 88, 90))
equipas.append(Team("Manchester United", 92, 90, 93))
equipas.append(Team("Paris Saint-Germain", 93, 90, 90))
equipas.append(Team("Monaco", 84, 83, 85))
equipas.append(Team("Bayern München", 91, 85, 90))
equipas.append(Team("BVB Dormund", 88, 85, 87))
equipas.append(Team("Juventus", 94, 90, 89))
equipas.append(Team("AC Milan", 86, 80, 82))
equipas.append(Team("Ajax", 79, 77, 85))
equipas.append(Team("Arsenal", 82, 80, 84))
equipas.append(Team("Atlético Madrid", 82, 88, 80))
equipas.append(Team("FC Zenit", 80, 75, 76))
#
#
# class Player(object):
#
#     def __init__(self, name):
#         self.name = name
#
# playerlist.append(Player("Jonas"))
# playerlist.append(Player("Seferovic"))
# playerlist.append(Player("Zivkovic"))
#
#
#
# equipas.append(Team("Real Madrid", playerlist, 95, 90, 95))
# equipas.append(Team("Barcelona", 92, 88, 94))
# equipas.append(Team("Benfica", 85, 79, 89))
# equipas.append(Team("FC Porto", 82, 80, 85))
#
#
random.shuffle(equipas)


for i, _ in enumerate(equipas):
    for j in range(0, len(equipas)):
        if i != j:
            plays = numberevents(equipas[i].teamplay, equipas[j].teamplay)
            dif_attack = float(equipas[i].attack - equipas[j].attack)
            homeplays, awayplays = getplays(plays, dif_attack)

            homefatigue = 0
            awayfatigue = 0

            homegoals, awaygoals = match(homeplays, awayplays, equipas[i], equipas[j], homefatigue, awayfatigue)
            # homegoals, fatigue = event(int(homeplays), equipas[i], equipas[j], 0)
            # awaygoals, fatigue = event(int(awayplays), equipas[j], equipas[i], 0)

            print("%s %s - %s %s" % equipas[i].name, homegoals, awaygoals, equipas[j].name)

            if homegoals > awaygoals:
                equipas[i].points += 3
                equipas[i].wins += 1
                equipas[j].losses += 1
            elif awaygoals > homegoals:
                equipas[j].points += 3
                equipas[j].wins += 1
                equipas[i].losses += 1
            else:
                equipas[i].points += 1
                equipas[i].draws += 1
                equipas[j].points += 1
                equipas[j].draws += 1

equipas.sort(key=attrgetter('points'), reverse=True)

for z, _ in enumerate(equipas):
    print('%20s \t %s points\t\t\t wins: %s draws: %s losses: %s') % (equipas[z].name, equipas[z].points, equipas[z].wins, equipas[z].draws, equipas[z].losses)
    # for u,_ in enumerate(equipas[z].playerlist):
    #     print('%s') % (equipas[z].playerlist[u].name)