from functions_escolha import *


equipas = list()
global selectedTeam


class Team(object):
    def __init__(self, name, attack, defence, teamplay):
        self.name = name
        self.attack = attack
        self.defence = defence
        self.teamplay = teamplay

equipas.append(Team("Real Madrid", 95, 90, 95))
equipas.append(Team("Barcelona", 92, 88, 94))
equipas.append(Team("Benfica", 85, 79, 89))
equipas.append(Team("FC Porto", 82, 80, 85))
equipas.append(Team("Chelsea", 90, 88, 90))
equipas.append(Team("Manchester United", 92, 90, 93))
equipas.append(Team("Paris Saint-Germain", 93, 90, 90))
equipas.append(Team("Monaco", 84, 83, 85))
equipas.append(Team("Bayern München", 91, 85, 90))
equipas.append(Team("BVB Dormund", 88, 85, 87))
equipas.append(Team("Juventus", 94, 90, 89))
equipas.append(Team("AC Milan", 86, 80, 82))
equipas.append(Team("Ajax", 79, 77, 85))
equipas.append(Team("Arsenal", 82, 80, 84))
equipas.append(Team("Atlético Madrid", 82, 88, 80))
equipas.append(Team("FC Zenit", 80, 75, 76))



def doFixture(contenders, modos, jornada):

# for j, _ in enumerate(contenders):
#     if contenders[j].name == selectedTeam.name:
#         index = j

for i, in range(0, len(contenders) - jornada):
    plays = numberevents(contenders[i].teamplay, contenders[i + jornada].teamplay)
    dif_attack = float(contenders[i].attack - contenders[i + jornada].attack)

    homeplays, awayplays = getplays(plays, dif_attack)
    homefatigue = 0
    awayfatigue = 0

    homegoals, awaygoals = match(homeplays, awayplays, contenders[i], contenders[i + jornada], homefatigue, awayfatigue,
                                 modos)

    homeTeam = contenders[i].name
    awayTeam = contenders[i + jornada].name

for i, _ in enumerate(contenders):
    if i % 2 == 0:
        # matchScore = event(contenders[i], contenders[i + 1])
        plays = numberevents(contenders[i].teamplay, contenders[i + 1].teamplay)
        dif_attack = float(contenders[i].attack - contenders[i + 1].attack)

        homeplays, awayplays = getplays(plays, dif_attack)


        homefatigue = 0
        awayfatigue = 0

        homegoals, awaygoals = match(homeplays, awayplays, contenders[i], contenders[i+1], homefatigue, awayfatigue,
                                     modos)

        # homegoals, homefatigue = event(int(homeplays), contenders[i], contenders[i + 1], 0)
        # awaygoals, awayfatigue = event(int(awayplays), contenders[i + 1], contenders[i], 0)

        homeTeam = contenders[i].name
        awayTeam = contenders[i + 1].name

        if homegoals > awaygoals:
            winners.append(contenders[i])

            print('%25s %s - %s %s' % (homeTeam, str(homegoals), str(awaygoals), awayTeam))
        # guarantee that no result ends in a tie in the knockout round
        elif homegoals < awaygoals:
            winners.append(contenders[i + 1])

            print('%25s %s - %s %s' % (homeTeam, str(homegoals), str(awaygoals), awayTeam))
        else:
            extratime_homegoals, extratime_awaygoals = match(homeplays/3, awayplays/3, contenders[i], contenders[i + 1], homefatigue, awayfatigue,
                                                             modos)
            # extratime_awaygoals, fatigue = event(plays/3, contenders[i + 1], contenders[i], awayfatigue)
            if extratime_homegoals > extratime_awaygoals:
                winners.append(contenders[i])
                print('%25s %s - %s %s (%s-%s after extra time)' % (
                homeTeam, homegoals, awaygoals, awayTeam, homegoals + extratime_homegoals,
                awaygoals + extratime_awaygoals))
            elif extratime_awaygoals > extratime_homegoals:
                winners.append(contenders[i + 1])
                print('%25s %s - %s %s (%s-%s after extra time)' % (
                homeTeam, homegoals, awaygoals, awayTeam, homegoals + extratime_homegoals,
                awaygoals + extratime_awaygoals))
            else:
                homepenalties, awaypenalties = penalties()
                print('%25s %s - %s %s (%s-%s after extra time) (%s-%s in penalties)' % (
                    homeTeam, homegoals, awaygoals, awayTeam, homegoals + extratime_homegoals,
                    awaygoals + extratime_awaygoals, homepenalties, awaypenalties))
                if homepenalties > awaypenalties:
                    winners.append(contenders[i])
                else:
                    winners.append(contenders[i+1])


if __name__ == '__main__':
    doFixture(equipas)