import pygame
import os

dir = os.path.dirname(__file__) + "/media pygame"

_gameWidth = 400
_gameHeight = 300
_fps = 60


def getGameWidth():
    return _gameWidth


def getGameHeight():
    return _gameHeight


def run_game(starting_scene):
    pygame.init()

    global font
    font = pygame.font.Font(dir + '\BRUSHCUT.TTF', 24)
    screen = pygame.display.set_mode((_gameWidth, _gameHeight))
    clock = pygame.time.Clock()

    active_scene = starting_scene

    while active_scene is not None:
        pressed_keys = pygame.key.get_pressed()

        # Event filtering
        filtered_events = []
        for event in pygame.event.get():
            quit_attempt = False
            if event.type == pygame.QUIT:
                quit_attempt = True
            elif event.type == pygame.KEYDOWN:
                alt_pressed = pressed_keys[pygame.K_LALT] or \
                              pressed_keys[pygame.K_RALT]
                if event.key == pygame.K_ESCAPE:
                    quit_attempt = True
                elif event.key == pygame.K_F4 and alt_pressed:
                    quit_attempt = True

            if quit_attempt:
                active_scene.terminate()
            else:
                filtered_events.append(event)

        active_scene.processInput(filtered_events, pressed_keys)
        active_scene.update()
        active_scene.render(screen)

        active_scene = active_scene.next

        pygame.display.flip()
        clock.tick(_fps)

        # The rest is code where you implement your game using the Scenes model
