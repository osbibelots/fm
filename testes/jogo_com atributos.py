#! /usr/bin/env python
#-*- coding: utf-8 -*-

import random
from random import randint

teams = list()
a = 0
winners = list()


class Team(object):
    def __init__(self, name, str):
        self.name = name
        self.str = str

teams.append(Team("Real Madrid", 99))
teams.append(Team("Benfica", 80))

#
while a < 100:
    r = random.random()

    for i, _ in enumerate(teams):
        if i % 2 == 0:
            # winpercentage = teams[i].str/(teams[i].str + teams[i + 1].str)
            dif = teams[i].str - teams[i - 1].str
            winpercentage = (2 * (dif) * (2**(dif/10)))/100
            hometeam = teams[i].name
            awayteam = teams[i + 1].name

            if r < winpercentage:
                resultado_casa = str(randint(1, 6))
                resultado_fora = str(randint(0, int(resultado_casa) - 1))
                winners.append(hometeam)
            else:
                resultado_fora = str(randint(1, 6))
                resultado_casa = str(randint(0, int(resultado_fora) - 1))
                winners.append(awayteam)

            #print(hometeam + " " + resultado_casa + " vs " + resultado_fora + " " + awayteam)

            a += 1

for j, _ in enumerate(teams):
    print(teams[j].name + " ganhou " + str(winners.count(teams[j].name)))

print(winpercentage)

print("Terminou")