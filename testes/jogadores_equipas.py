#! /usr/bin/env python
# -*- coding: utf-8 -*-


class Player:
    def __init__(self, name, age, skill):
        self.name = name
        self.age = age
        self.skill = skill

    def __str__(self):
        return f"{self.name} {self.age} {self.skill}"


class Team:
    def __init__(self, name, players=None):
        self.name = name
        if players is not None:
            self._players = list(players)
        else:
            self._players = []

    def add_player(self, obj):
        if isinstance(obj, Player):
            self._players.append(obj)
        else:
            print("Please provide player object")

    def __iter__(self):
        return iter(self._players)

    def __str__(self):
        out = [f"Team name: {self.name}", "Players:"]
        out.extend(str(player) for player in self)
        return "\n".join(out)

    def get_number_players(self):
        return len(self._players)

if __name__ == "__main__":

    equipa1 = [Player("Preto", 26, 9),
               Player("Carlos", 26, 9),
               Player("Guedes", 26, 8),
               Player("Fraga", 23, 8),
               Player("João Pires", 24, 8)]

    sabado = Team("Futebol Sábado", equipa1)
    #print(sabado)

    equipa2 = [Player("André", 23, 4),
               Player("Toni", 26, 5),
               Player("Buda", 26, 6),
               Player("Cotrim", 23, 7),
               Player("Félix", 24, 8)]

    # equivalent:

    for item in equipa2:
        sabado.add_player(item)

    #print(sabado)

    bcn = [Player("Messi", 30, 10),
               Player("Suarez", 28, 10),
               Player("Neymar", 26, 10),
               Player("Piqué", 32,7),
               Player("Ter Stegen", 27, 8)]

    Barcelona = Team("Barcelona", bcn)
    idade = 0

    for i in Barcelona._players:
        idade += i.age
    print(idade/Barcelona.get_number_players())


    #print(Barcelona._players[0].age)
