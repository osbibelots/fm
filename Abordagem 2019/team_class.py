#! /usr/bin/env python
# -*- coding: utf-8 -*-

from player_class import *
import pandas as pd
import random


class Team:
    """
    Classe que cria equipa com nome e array de jogadores
    """
    def __init__(self, name, league, country, players=None):
        self.name = name
        self.league = league
        self.cup = None
        self.country = country
        if players is not None:
            self._players = list(players)
        else:
            self._players = []
        self.points = 0
        self.matches = 0
        self.goals_scored = 0
        self.goals_conceded = 0
        self.wins = 0
        self.draws = 0
        self.losses = 0
        self.attack = 0
        self.defence = 0
        self.morale = 50
        self.fatigue = None
        self.budget = 0
        self.groupstats = {"goals_scored": 0, "goals_conceded": 0, "wins": 0, "draws": 0, "losses": 0, "points": 0,
                           "matches": 0}
        self.trainingpoints = 0

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __len__(self):
        return len(self._players)

    def __call__(self):
        return self.name

    def __eq__(self, other):
        """
        compara equipas, se não for instance dá logo False,
        se tiver outro nome, dá false também
        :param other:
        :return:
        """
        if isinstance(other, Team):
            return self.name == other.name
        return False

    def setfatigue(self):
        totalfatigue = 0
        for player in self._players:
            totalfatigue += player.fatigue
        self.fatigue = totalfatigue / len(self)

    def add_fatigue(self, value):
        for player in self._players:
            player.add_fatigue(value)
        self.setfatigue()

    def remove_fatigue(self, value, days):
        for player in self._players:
            player.remove_fatigue(value, days)
        self.setfatigue()

    def list_players(self):
        print(self._players)

    def remove_player(self, player):
        """
        :param player: tem de ser objecto
        :return:
        """
        for idx, i in enumerate(self._players):
            if player == i:
                del self._players[idx]

    def add_player(self, player):
        """
        :param player: tem de ser objecto
        :return:
        """
        self._players.append(player)

    def setattack(self):
        """
        média do ataque de todos os jogadores menos GK
        :return:
        """
        totalattack = 0
        for player in self._players:
            if player.position == 'GK':
                pass
            else:
                totalattack += player.attack
        self.attack = totalattack / (len(self) - 1)

    def setdefence(self):
        """
        média da defesa
        :return:
        """
        totaldefence = 0
        for player in self._players:
            totaldefence += player.defence
        self.defence = totaldefence / len(self)

    # TODO: colocar estes boosts e sets todos na mesma funcao, que recebe o atributo a modificar
    def boostattack(self):
        self.attack = self.attack * 1.1
        self.defence = self.defence * 0.9

    def boostdefence(self):
        self.defence = self.defence * 1.1
        self.attack = self.attack * 0.9

    def get_position(self, position):
        group_players = list()
        for player in self._players:
            if player.position == position:
                group_players.append(player)
        return group_players

    def get_formation(self):
        """
        Tem como objectivo saber a formação utilizada pela equipa
        :return: array de posições em string
        """
        formation = list()
        for player in self._players:
            formation.append(player.position)
        return formation

    def cleanseason(self):
        """
        Limpa todas as estatisticas da época para começar a seguinte, da equipa e jogadores
        :return: nada
        """
        self.points, self.matches = 0, 0
        self.goals_scored, self.goals_conceded = 0, 0
        self.wins, self.draws, self.losses = 0, 0, 0

        for player in self._players:
            player.cleanseason()

        self.clean_europeancup()

    def update_trainingpoints(self, value):
        self.trainingpoints += value

    def generate_squadlist(self):
        data = list()
        for player in self._players:
            data.append([player, player.position, player.fatigue, player.matches, player.goals, player.attack, player.defence])

        table = pd.DataFrame(data, columns=['Player', 'Position', 'Fatigue', 'Played', 'Goals', 'Attack', 'Defence'])

        table = table.sort_values(by=['Goals', 'Attack', 'Defence'], ascending=[False, False, True])

        table_str = table.to_string()
        return table, table_str

    def print_squadlist(self):
        _, table_str = self.generate_squadlist()
        return print('\n' + table_str)

    def add_goals(self, scored, conceded):
        """
        adiciona golos marcados e sofridos bem como os pontos (tipicamente após um jogo)
        :param scored:
        :param conceded:
        :return:
        """
        self.goals_scored += scored
        self.goals_conceded += conceded
        self.matches += 1
        # adiciona um jogo a cada jogador da equipa
        for player in self._players:
            player.add_match()

        if scored > conceded:
            self.points += 3
            self.wins += 1
            self.moralechange(5 + int((scored - conceded) * 0.5))
            self.update_trainingpoints(1)
        elif scored == conceded:
            self.points += 1
            self.draws += 1
        else:
            self.losses += 1
            self.moralechange(-(5 + int((conceded - scored) * 0.5)))

    def add_groupgoals(self, scored, conceded):
        self.groupstats["goals_scored"] += scored
        self.groupstats["goals_conceded"] += conceded
        self.groupstats["matches"] += 1
        if scored > conceded:
            self.groupstats["points"] += 3
            self.groupstats["wins"] += 1
            self.moralechange(5 + int((scored - conceded) * 0.5))
        elif scored == conceded:
            self.groupstats["points"] += 1
            self.groupstats["draws"] += 1
        else:
            self.groupstats["losses"] += 1
            self.moralechange(-(5 + int((conceded - scored) * 0.5)))

    def clean_europeancup(self):
        self.groupstats = {"goals_scored": 0, "goals_conceded": 0, "wins": 0, "draws": 0, "losses": 0, "points": 0,
                           "matches": 0}

    def resetattributes(self):
        """
        redefine os atributos com base nos atributos base dos jogadores e na moral,
        útil para retirar eventuais boosts
        :return:
        """
        self.setattack()
        self.setdefence()
        self.moraleinfluence()

    def moralechange(self, value):
        """
        altera o valor da moral, entre 0 e 100
        :param value: valor a aumentar/diminuir
        :return:
        """
        self.morale += value
        if self.morale > 100:
            self.morale = 100
        elif self.morale < 0:
            self.morale = 0

    def moraleinfluence(self):
        """
        alteracao dos atributos com base na moral
        :return:
        """
        self.attack = ((0.001 * self.morale - 0.05) + 1) * self.attack
        self.defence = ((0.001 * self.morale - 0.05) + 1) * self.defence

    def goalscorer_chance(self):
        """
        Calcula a prob que cada jogador tem de marcar
        tendo em conta a posição e o ataque
        :return: array de probabilidades cuja soma é 1
        """
        # ponderador de posição
        positionchance = {
            "GK": 0,
            "DC": 800, "DL": 1000, "DR": 1000,
            "DMC": 900, "MC": 1400, "AMC": 1600,
            "AML": 1700, "AMR": 1700,
            "ST": 2000
        }
        goalchance = list()
        attackfactor, finishingfactor = 0.2, 0.8
        for key, value in positionchance.items():
            for player in self._players:
                if player.position == key:
                    goal_attributes = player.attack * attackfactor + player.finishing * finishingfactor
                    goalchance.append(goal_attributes * value)

        total = sum(goalchance)
        for i in range(0, len(goalchance)):
            if i > 0:
                goalchance[i] = goalchance[i] / total + goalchance[i - 1]
            else:
                goalchance[i] = goalchance[i] / total

        return goalchance

    def goalscorer(self):
        """
        usa o array de probabilidades criado pelo goalscorer_chance para descobrir o jogador que marca efectivamente
        :return: jogador que marca
        """
        goalchance = self.goalscorer_chance()
        r = random.random()
        for i, item in enumerate(goalchance):
            if i == 0:
                lowlimit = 0
            else:
                lowlimit = goalchance[i - 1]

            if lowlimit < r < item:
                idxscorer = i
                break

        return self._players[idxscorer]

    def scorer_list(self):
        """
        Gera uma lista de marcadores da equipa
        :return: dataframe e dataframe em string
        """
        data = list()
        for player in self._players:
            data.append([player.name, self.name, player.position, player.goals])

        # colunas renomeadas, lista ordenada por golos, e são retirados os que têm 0 golos
        table = pd.DataFrame(data, columns=['Player', 'Team', 'Position', 'Goals'])
        table = table.sort_values(by=['Goals'], ascending=[False])
        table = table[(table != 0).all(1)]

        # gera uma coluna nova com as posições na tabela
        table = table.assign(Top=[1 + i for i in range(len(table))])[['Top'] + table.columns.tolist()]
        table = table.set_index('Top')
        table_str = table.to_string()
        return table_str, table

    def random_player(self):
        return random.choice(self._players)

    @property
    def teamplay(self):
        """
        média do overall
        :return:
        """
        totalteamplay = 0
        for player in self._players:
            totalteamplay += player.teamplay
        return totalteamplay / len(self)

    @property
    def stamina(self):
        """
        média da stamina
        :return:
        """
        totalstamina = 0
        for player in self._players:
            totalstamina += player.stamina
        return totalstamina / len(self)





