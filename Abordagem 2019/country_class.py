#! /usr/bin/env python
# -*- coding: utf-8 -*-


class Country:
    def __init__(self, name):
        self.name = name
        self.league = None
        self.cup = None

    def __repr__(self):
        return self.name

    def add_league(self, league):
        self.league = league

