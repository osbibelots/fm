#! /usr/bin/env python
# -*- coding: utf-8 -*-


from team_class import *
from player_class import *
import pandas as pd
from time import sleep
from user_class import *
from aux_functions import *


class Match:
    def __init__(self, hometeam, awayteam, neutral=None):
        self.hometeam = hometeam
        self.awayteam = awayteam
        self.homegoals, self.ethomegoals = 0, 0
        self.awaygoals, self.etawaygoals = 0, 0
        self.homeplays = 0
        self.awayplays = 0
        self.currentplays, self.etcurrentplays = 1, 1
        self.totalplays, self.extratimeplays = 0, 0
        self.fatigue = [self.hometeam.fatigue, self.awayteam.fatigue]
        self.homescorers = []
        self.awayscorers = []
        self.scorer = ''
        self.scorerteam = ''
        self.homeshotstarget, self.awayshotstarget = 0, 0
        self.homeshots, self.awayshots = 0, 0
        self.shots, self.shotstarget = 0, 0
        self.homepenalties, self.awaypenalties = 0, 0
        self.neutral = neutral
        self.winner, self.loser = None, None

    def generate_table(self):
        """
        apresenta uma tabela com dados do jogo
        :return:
        """
        datahome = [self.homegoals, self.homeplays, self.homeshots, self.homeshotstarget]
        dataaway = [self.awaygoals, self.awayplays, self.awayshots, self.awayshotstarget]
        titles = ['Goals', 'Plays', 'Shots', 'Shots on Target']
        df = pd.DataFrame({str(self.hometeam): datahome, str(self.awayteam): dataaway})
        df.insert(loc=0, column='', value=titles)
        df = df.to_string(index=False)
        print(df)

    def get_totalplays(self):
        """
        calcula o numero de jogadas que o jogo vai ter consoante o atributo teamplay das duas equipas
        """
        baseplays = 24
        plays = baseplays + 2.6*cube(self.hometeam.attack - self.awayteam.defence) + \
                        2.5*cube(self.awayteam.attack - self.hometeam.defence)

        plays = int(plays)
        self.totalplays = plays
        self.extratimeplays = int(plays / 3)

    def get_play(self, et):
        """
        dependendo da diferença da qualidade das equipas, vê o numero de jogadas de cada uma
        """

        # Basicamente se os teamplays forem 90 home e 10 away fica:
        # (90 - 10) / 90 = 8/9
        # as jogadas totais sao = (1 + 8/9)x + x
        # porque no fundo sao as jogadas de x + uma percentagem acima em que x é o numero de jogadas do mais fraco
        # o 1 + 8/9 é o playfactor
        # para descobrir x é preciso fazer plays a dividir por (1 + 8/9) + 1
        dif = self.hometeam.teamplay - self.awayteam.teamplay
        higher = max(self.hometeam.teamplay, self.awayteam.teamplay)
        playfactor = 1 + (abs(dif) / higher)
        scalefactor = 1.7
        denominator = playfactor * scalefactor + 1
        if dif >= 0:
            perc_home = 1 - 1 / denominator
        else:
            perc_home = 1 / denominator

        # equipa da casa terá x% probabilidades de ganhar a jogada, equipa fora terá 100 - x%

        r = random.random()
        if et:
            self.etcurrentplays += 1
        else:
            self.currentplays += 1
        if r <= perc_home:
            home, away = True, False
            self.homeplays += 1

        else:
            away, home = True, False
            self.awayplays += 1

        return home, away

    def simulate_play(self, et):
        """
        Simula uma jogada
        :return:
        """
        home, away = self.get_play(et)
        homeboost = 0
        if home:
            if not self.neutral:
                # se for estadio neutro nao deve haver homeboost
                homeboost = 0.008
            attackteam, defenceteam = self.hometeam, self.awayteam
        else:
            attackteam, defenceteam = self.awayteam, self.hometeam

        r = random.random()
        gkdefence = defenceteam.get_position('GK')[0].goalkeeping
        gk_cc = -8.16 * 10 ** -5 * gkdefence + 4.08 * 10 ** -3
        # assim gk com 99 defesa baixam a prob em 0.004 e gk com 50 nao alteram a prob
        goalchance = 0.08 + \
                     (((attackteam.attack - defenceteam.defence) / 2) * 0.002) + \
                     ((self.fatigue[0] - self.fatigue[1]) * 0.002) + gk_cc + homeboost
        # percentagens calculadas com base na analise das 5 melhores ligas europeias ao longo de 5 anos
        if r < 0.5:
            self.shots += 1
            if home:
                self.homeshots += 1
            else:
                self.awayshots += 1
        if r < 0.3:
            self.shotstarget += 1
            if home:
                self.homeshotstarget += 1
            else:
                self.awayshotstarget += 1
        if r < goalchance:
            scorer = attackteam.goalscorer()
            scorer.add_goal()
            self.scorer = scorer
            self.scorerteam = attackteam

            if home:
                self.homescorers.append(scorer)
                if et:
                    self.ethomegoals += 1
                else:
                    self.homegoals += 1
            else:
                self.awayscorers.append(scorer)
                if et:
                    self.etawaygoals += 1
                else:
                    self.awaygoals += 1

        # ao fazer uma jogada a fadiga aumenta
        defenceteam.add_fatigue(1)
        attackteam.add_fatigue(1)

    def minute_play(self, duration):
        """
        gera vector de minutos com o mesmo tamanho do numero de jogadas
        cada jogada tem correspondencia com um minuto
        :param duration: numero de minutos
        :return:
        """
        minutes = list()
        for x in range(1, self.totalplays + 1):
            minutes.append(random.randint(1, duration))
        minutes = sorted(minutes)

        return minutes

    def setwinner(self):
        """
        define vencedor e perdedor do jogo, em caso de empate permanecem a None
        :return:
        """
        totalhomegoals = self.homegoals + self.ethomegoals + self.homepenalties
        totalawaygoals = self.awaygoals + self.etawaygoals + self.awaypenalties
        if totalhomegoals > totalawaygoals:
            self.winner = self.hometeam
            self.loser = self.awayteam
        elif totalhomegoals < totalawaygoals:
            self.winner = self.awayteam
            self.loser = self.hometeam

        if (self.hometeam or self.awayteam) == user.team:
            user.savelastmatch(self)

    def printresult(self):
        """
        faz print do resultado do jogo
        :return:
        """
        if self.homepenalties > 0 or self.awaypenalties > 0:
            print("%s %s - %s %s (%s - %s in penalties)" % (self.hometeam, self.homegoals, self.awaygoals, self.awayteam,
                                                            self.homepenalties, self.awaypenalties))
        elif self.ethomegoals + self.etawaygoals > 0 and self.ethomegoals != self.etawaygoals:
            print("%s %s - %s %s (%s - %s in extra time)" % (self.hometeam, self.homegoals, self.awaygoals, self.awayteam,
                                                             self.homegoals + self.ethomegoals,
                                                             self.awaygoals + self.etawaygoals))
        else:
            print("%s %s - %s %s" % (self.hometeam, self.homegoals, self.awaygoals, self.awayteam))

    def setsecondleg(self, firstleghg, firstlegag, user=False):
        if firstleghg == self.homegoals and firstlegag == self.awaygoals:
            if user:
                self.extratimeuser(secondleg=True)
            else:
                self.extratime(secondleg=True)
        elif firstleghg + self.awaygoals > firstlegag + self.homegoals:
            self.winner = self.awayteam
            self.loser = self.hometeam
        elif firstleghg + self.awaygoals < firstlegag + self.homegoals:
            self.winner = self.hometeam
            self.loser = self.awayteam
        elif (firstleghg + self.awaygoals == firstlegag + self.homegoals) and (firstlegag > self.awaygoals):
            self.winner = self.hometeam
            self.loser = self.awayteam
        elif firstleghg + self.awaygoals == firstlegag + self.homegoals and firstlegag < self.awaygoals:
            self.winner = self.awayteam
            self.loser = self.hometeam

    def complete_match(self, et=False, secondleg=False, firstleghg=0, firstlegag=0, detail=False):
        """
        simula o jogo inteiro sem pausas, jogo nao utilizador
        :param et: se True vai a prologamento dado certas condiçoes
        :param secondleg:
        :param firstleghg: golos da primeira mao em casa, se nao houver, defeito é 0
        :param firstlegag: golos da primeira mao fora, se nao houver, defeito é 0
        :param detail: mostra resultado se True
        :return:
        """
        self.get_totalplays()
        while self.currentplays <= self.totalplays:
            self.simulate_play(et=False)

        if secondleg:
            # caso em que as duas maos sao iguais em resultados (2x0 primeira mao e 2x0 segunda mao pex)
            self.setsecondleg(firstleghg, firstlegag)

        elif et and self.homegoals == self.awaygoals:
            self.extratime()

        if not secondleg:
            self.setwinner()

        if detail:
            self.printresult()

    def tactic(self):
        """
        input do utilizador quanto à táctica a usar
        :return:
        """
        print('\n\tTactic:\n\t1: Attack\n\t2: Neutral\n\t3: Defence\n')
        tactic = input('Input: ')
        if tactic == '1':
            if user.team == self.hometeam:
                self.hometeam.boostattack()
            else:
                self.awayteam.boostattack()
        elif tactic == '3':
            if user.team == self.hometeam:
                self.hometeam.boostdefence()
            else:
                self.awayteam.boostdefence()

    def key_match(self, et=False, secondleg=False, firstleghg=0, firstlegag=0):
        """
        jogo de utilizador
        """
        print('\n%s vs %s! Players are ready for kick-off!\n' % (self.hometeam, self.awayteam))
        self.tactic()
        self.get_totalplays()
        minutes = self.minute_play(90)

        while self.currentplays <= self.totalplays:
            i = self.currentplays
            goals = self.homegoals + self.awaygoals
            self.simulate_play(et=False)
            if goals < self.homegoals + self.awaygoals:
                print('GOOOOOOOOOOOOOAAAAAAAAAAL %s \n%s (%i minutes)' %
                      (self.scorerteam, self.scorer, minutes[i - 1]))
                print('%s %s - %s %s' % (self.hometeam, self.homegoals, self.awaygoals, self.awayteam))
                sleep(1)
            if i < len(minutes):
                if minutes[i] > 45 and minutes[i - 1] <= 45:
                    print('\nHALF-TIME\n')
                    self.hometeam.resetattributes()
                    self.awayteam.resetattributes()
                    self.tactic()

        self.hometeam.resetattributes()
        self.awayteam.resetattributes()
        if secondleg:
            self.setsecondleg(firstleghg, firstlegag, user=True)
        elif et and self.homegoals == self.awaygoals:
            self.extratimeuser()
        else:
            print('The match is over!\n')

        self.setwinner()

    def extended_match(self):
        """
        pára em outras jogadas além do golo
        :return:
        """
        pass

    def setsecondlegextratime(self):
        if self.ethomegoals + self.awaygoals == 0:
            self.penalties()
            if self.homepenalties > self.awaypenalties:
                self.winner = self.hometeam
                self.loser = self.awayteam
            else:
                self.winner = self.awayteam
                self.loser = self.hometeam
        elif self.ethomegoals == self.etawaygoals:
            self.winner = self.awayteam
            self.loser = self.hometeam
        elif self.ethomegoals > self.etawaygoals:
            self.winner = self.hometeam
            self.loser = self.awayteam
        elif self.ethomegoals < self.etawaygoals:
            self.winner = self.awayteam
            self.loser = self.hometeam

    def extratime(self, secondleg=False):
        while self.etcurrentplays <= self.extratimeplays:
            self.simulate_play(et=True)

        if secondleg:
            self.setsecondlegextratime()
        else:
            if self.ethomegoals == self.etawaygoals:
                self.penalties()

    def extratimeuser(self, secondleg=False):

        print('Extra Time!\n')
        self.tactic()
        minutes = self.minute_play(30)
        while self.etcurrentplays <= self.extratimeplays:
            i = self.etcurrentplays
            goals = self.ethomegoals + self.etawaygoals
            self.simulate_play(et=True)
            if goals < self.ethomegoals + self.etawaygoals:

                print('GOOOOOOOOOOOOOAAAAAAAAAAL %s \n%s (%i minutes ET)' %
                      (self.scorerteam, self.scorer, minutes[i - 1]))
                print('%s %s - %s %s' % (self.hometeam, self.homegoals + self.ethomegoals,
                                         self.awaygoals + self.etawaygoals, self.awayteam))
                sleep(1)
            if i < len(minutes):
                if minutes[i] > 15 and minutes[i - 1] <= 15:
                    print('\nHALF-TIME\n')
                    self.hometeam.resetattributes()
                    self.awayteam.resetattributes()
                    self.tactic()

        print('The match is over!\n')
        self.hometeam.resetattributes()
        self.awayteam.resetattributes()

        if secondleg:
            self.setsecondlegextratime()
        else:
            if self.ethomegoals == self.etawaygoals:
                self.penaltiesuser()

    def penalty(self):
        scored = 0
        r = random.random()
        goalchance = 0.8
        if r < goalchance:
            scored = 1
        return scored

    def penaltyrounduser(self):
        """
        uma ronda de penalties
        :param detail: o detail aqui é para fazer os prints, ou seja, ver detalhadamente
        :return:
        """
        homepenalty = self.penalty()
        self.homepenalties += homepenalty
        if homepenalty == 1:
            print('GOALLLL\n%s %s - %s %s' % (self.hometeam, self.homepenalties, self.awaypenalties, self.awayteam))
            sleep(1)
        elif homepenalty == 0:
            print('And he misses!!!\n%s %s - %s %s' % (self.hometeam, self.homepenalties, self.awaypenalties,
                                                       self.awayteam))
            sleep(1)
        awaypenalty = self.penalty()
        self.awaypenalties += awaypenalty
        if awaypenalty == 1:
            print('GOAAAAAL\n%s %s - %s %s' % (self.hometeam, self.homepenalties, self.awaypenalties, self.awayteam))
            sleep(1)
        elif awaypenalty == 0:
            print('And he misses!!!\n%s %s - %s %s' % (self.hometeam, self.homepenalties, self.awaypenalties,
                                                       self.awayteam))
            sleep(1)

    def penaltiesuser(self):
        """
        5 penalties iniciais e depois mata-mata
        :return:
        """
        for i in range(0, 5):
            self.penaltyrounduser()
        while self.homepenalties == self.awaypenalties:
            self.penaltyrounduser()

    def penaltyround(self):
        """
        uma ronda de penalties
        :param detail: o detail aqui é para fazer os prints, ou seja, ver detalhadamente
        :return:
        """
        homepenalty = self.penalty()
        self.homepenalties += homepenalty
        awaypenalty = self.penalty()
        self.awaypenalties += awaypenalty

    def penalties(self):
        """
        5 penalties iniciais e depois mata-mata
        :return:
        """
        for i in range(0, 5):
            self.penaltyround()
        while self.homepenalties == self.awaypenalties:
            self.penaltyround()
