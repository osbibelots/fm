#! /usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
from datetime import timedelta
from user_class import *
from load import *


class Calendar:
    def __init__(self):
        """
        Semana inicial (2ª semana de Agosto)
        """
        self.season = 1
        self.week = 0  # tem de ser 31 senao as ligas nao começam na altura devida
        self.year = 0
        self.day = datetime(2018, 8, 5)
        self.getyearweek()

    def increment_season(self):
        self.season += 1
        for player in load.playerlist:
            player.add_age(1)
            player.attribute_development()

    def getyearweek(self):
        self.year, self.week, _ = self.day.isocalendar()

    def add_day(self, value):
        """isto so é usado no add_week"""
        self.day = self.day + timedelta(days=value)
        self.getyearweek()
        for team in load.teamlist:
            team.remove_fatigue(5, value)

    def add_week(self):
        """
        quando chega ao fim do ano volta ao inicio, na semana 22 incrementa a epoca para todas as ligas e taças
        se houver jogos da equipa do user, nao avança 7 dias, avança só ate ao proximo jogo
        :return:
        """
        for team in load.teamlist:
            if team.trainingpoints > 0 and team is not user.team:
                training.random_attribute_change(team)

        datelist = sorted(i for i in user.matchweek if i > self.day)
        if len(datelist) != 0:
            if (datelist[0] - self.day).days < 7:
                self.add_day((datelist[0] - self.day).days)
            else:
                self.add_day(7)
        else:
            self.add_day(7)

    def weekdayfromweek(self, week, dow):
        """
        :param dow: 0 Sunday, 1 Monday, 2 Tuesday etc...
        :return:
        """
        if week >= self.week + 1:
            year = self.year
        else:
            year = self.year + 1
        myDate = str(year) + " " + str(week)
        strdow = " " + str(dow)
        day = datetime.strptime(myDate + strdow, "%Y %W %w")
        return day

    def setschedule(self, weeks, dow):
        """
        gera os schedules aqui
        :param weeks:  array das semanas
        :param dow:
        :return:
        """
        dates = list()
        for week in weeks:
            day = calendar.weekdayfromweek(week, dow)
            dates.append(day)
        return dates


calendar = Calendar()
