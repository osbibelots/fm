#! /usr/bin/env python
# -*- coding: utf-8 -*-

from random import shuffle
from match_class import *


class Cup:
    def __init__(self, name, country, teams=None):
        self.name = name
        self.country = country
        if teams is not None:
            self._teams = list(teams)
        else:
            self._teams = []
        self.matchweek = list()
        self.round = 1
        self.season = 1
        self.winners = pd.DataFrame(columns=['Season', 'Winner', 'Runner Up'])
        self.winner = None
        self.runnerup = None
        self.finalists = list()

    def __repr__(self):
        return self.name

    def __len__(self):
        """
        retorna o numero de rondas da taça
        :return:
        """
        size = len(self._teams)
        _, idx = power2(size)
        return idx + 1

    def cleanteams(self):
        self._teams = list()

    def printwinnertable(self):
        table = pd.DataFrame(self.finalists, columns=['Season', 'Winner', 'Runner Up'])
        table = table.sort_values(by=['Season'], ascending=[True])
        table = table.set_index('Season')

        table_str = table.to_string()
        if table.empty:
            print('There are no winners yet!')
        else:
            print('\n%s' % self.name)
            print(table_str)

    def setwinners(self):
        row = [self.season, self.winner, self.runnerup]
        self.finalists.append(row)

    def add_team(self, team):
        self._teams.append(team)

    def increment_season(self):
        self.setwinners()
        self.season += 1
        self.round = 1

    def generate_schedule(self):
        self.matchweek = list()
        if len(self) == 5:
            cupweek = [46, 50, 3, 11, 21]
            for i in cupweek:
                self.matchweek.append(i)

    def generate_knockoutround(self, detail):
        """
        tem de ser potencia de 2, e tenho de ver quantos sobram, nao faço ideia de como fazer
        :return:
        """
        if detail:
            print('\n%s' % self.name)
        losers = list()
        shuffle(self._teams)

        size = len(self._teams)
        if detail:
            if size == 16:
                print('\nRound of 16')
            elif size == 8:
                print('\nQuarter Finals')
            elif size == 4:
                print('\nSemi Finals')
            elif size == 2:
                print('\nFinal')
            else:
                print('Round %s' % self.round)

        # verifica se o numero de equipas é potencia de 2, se sim vao todos, se nao ficam de fora n - size
        if size != 0 and ((size & (size - 1)) == 0):
            i = 0
        else:
            n, _ = power2(size)
            i = n - size

        while i < len(self._teams):
            hometeam, awayteam = self._teams[i], \
                                 self._teams[i + 1]
            if size == 2:
                match = Match(hometeam, awayteam, neutral=True)
            else:
                match = Match(hometeam, awayteam)

            # verificar se é a equipa do user
            if hometeam == user.team or awayteam == user.team:
                # jogo com detalhe se for a equipa do user
                match.key_match(et=True)
            else:
                match.complete_match(et=True, detail=detail)

            losers.append(match.loser)
            if size == 2:
                self.winner = match.winner
                self.runnerup = match.loser

            i += 2

        for team in losers:
            self._teams.remove(team)

        if len(self._teams) == 1:
            self.winner.moralechange(20)
            if detail:
                print('\n%s wins the %s!!' % (self.winner, self.name))

        self.round += 1


class EuropeanCup:
    def __init__(self, name, teams=None):
        self.name = name
        if teams is not None:
            self._teams = list(teams)
        else:
            self._teams = []
        self.groupmatchday = 1
        self.grouplist = list()
        self.teamspergroup = 4
        self.matchweek = None
        self.round = 1
        self.leg = 1
        self.winners = list()
        self.runnersup = list()
        self.firstleghomegoals = list()
        self.firstlegawaygoals = list()
        self.grouplosers = list()
        self.season = 2
        self.finalists = list()
        self.winner = None
        self.runnerup = None

    def __len__(self):
        """
        número de grupos
        :return:
        """
        return int(len(self._teams) / self.teamspergroup)

    def __call__(self, *args, **kwargs):
        for group in self.grouplist:
            return group()

    def __repr__(self):
        return self.name

    def add_team(self, team):
        self._teams.append(team)

    def roundname(self):
        if self.round == 1:
            return 'Round of 16'
        elif self.round == 2:
            return 'Quarter Finals'
        elif self.round == 3:
            return 'Semi Finals'
        elif self.round == 4:
            return 'Final'

    def generate_schedule(self):
        self.matchweek = list()
        cupweek = [38, 40, 43, 45, 48, 50, 7, 10, 15, 16, 18, 19, 22]
        for i in cupweek:
            self.matchweek.append(i)

    def printwinnertable(self):
        table = pd.DataFrame(self.finalists, columns=['Season', 'Winner', 'Runner Up'])
        table = table.sort_values(by=['Season'], ascending=[True])
        table = table.set_index('Season')

        table_str = table.to_string()
        if table.empty:
            print('There are no winners yet!')
        else:
            print('\n%s' % self.name)
            print(table_str)

    def setwinners(self):
        row = [self.season, self.winner, self.runnerup]
        self.finalists.append(row)

    def generate_matchday(self, detail):
        if detail:
            print('\n%s' % self.name)
        if self.groupmatchday <= (self.teamspergroup - 1) * 2:
            if detail:
                print('Matchday %s' % self.groupmatchday)
            self.groupround(detail=detail)  # processa jogo de grupo
            self.groupmatchday += 1
        else:
            if detail:
                name = self.roundname()
                print('%s\n' % name)
            if len(self._teams) == 2:
                self.knockoutround(detail=detail, neutral=True)
            else:
                self.knockoutround(detail=detail)
                if self.leg == 1:
                    self.leg += 1
                elif self.leg == 2:
                    self.leg = 1
                    self.round += 1

    def generategroups(self):
        """
        gera os grupos e insere equipas em cada grupo
        :return:
        """
        shuffle(self._teams)
        self.grouplist = list()
        names = 'ABCDEFGHIJKLMNOPQRSTUVXZ'

        for i in range(0, len(self)):
            self.grouplist.append(Group(name='Group ' + names[i]))
            for j in range(0, self.teamspergroup):
                k = j + i * self.teamspergroup
                self.grouplist[i].add_team(self._teams[k])
            i += 1

    def groupround(self, detail):
        for group in self.grouplist:
            group.round(detail=detail)

    def definepots(self):
        if self.round == 1:
            self.winners, self.runnersup = list(), list()
            for group in self.grouplist:
                self.winners.append(group[0])
                self.runnersup.append(group[1])
                self.grouplosers.append(group[2])
                self.grouplosers.append(group[3])
            for team in self.grouplosers:
                self._teams.remove(team)
        else:
            self.winners, self.runnersup = list(), list()
            nummatches = int(len(self._teams) / 2)
            for i in range(0, nummatches):
                self.winners.append(self._teams[i])
                self.runnersup.append(self._teams[i + nummatches])

        shuffle(self.winners)
        shuffle(self.runnersup)

    def incrementseason(self):
        self.setwinners()
        self.season += 1
        self.grouplist = list()
        self.winners = list()
        self.runnersup = list()
        self.grouplosers = list()
        self.groupmatchday = 1
        self.round = 1
        self._teams = list()

    def knockoutround(self, detail, neutral=False):
        """
        gera ronda de knockout
        :return:
        """
        if neutral:
            match = Match(self._teams[0], self._teams[1], neutral=neutral)
            match.complete_match(et=True, detail=detail)
            self.winner = match.winner
            self.runnerup = match.loser
            self.incrementseason()
            if detail:
                print('%s wins the %s!!!' % (self.winner, self.name))
        if self.leg == 1 and not neutral:
            self.definepots()
            for i in range(0, len(self.winners)):
                match = Match(self.winners[i], self.runnersup[i])
                if user.team == match.hometeam or user.team == match.awayteam:
                    match.key_match(et=False)
                else:
                    match.complete_match(et=False, detail=detail)
                self.firstleghomegoals.append(match.homegoals)
                self.firstlegawaygoals.append(match.awaygoals)
        if self.leg == 2 and not neutral:
            for i in range(0, len(self.winners)):
                match = Match(self.runnersup[i], self.winners[i])
                if user.team == match.hometeam or user.team == match.awayteam:
                    match.key_match(secondleg=True, firstleghg=self.firstleghomegoals[i],
                                    firstlegag=self.firstlegawaygoals[i])
                else:
                    match.complete_match(secondleg=True, firstleghg=self.firstleghomegoals[i],
                                         firstlegag=self.firstlegawaygoals[i], detail=detail)
                self._teams.remove(match.loser)
            self.firstleghomegoals, self.firstlegawaygoals = list(), list()


class Group:
    def __init__(self, name, teams=None):
        """
        nome é A, B, C...
        :param teams:
        """
        if teams is not None:
            self._teams = list(teams)
        else:
            self._teams = []
            self.name = name
            self.matchday = 1
            self.fixtures = None
            self.fixturescalendar()

    def __getitem__(self, item):
        table, _ = self.generate_table()
        return table.iloc[item, 0]

    def __repr__(self):
        return self.name

    def __call__(self):
        """
        Se chamares a liga, ela retorna a tabela de pontuações
        :return: tabela de pontos
        """
        _, table = self.generate_table()
        return str(self.name) + '\n' + table

    def add_team(self, team):
        self._teams.append(team)

    def fixturescalendar(self):
        """
        calendario de confrontos de indices de equipas
        :return:
        """
        self.fixtures = [[[4, 2], [3, 1]], [[1, 4], [2, 3]], [[4, 3], [2, 1]], [[3, 4], [1, 2]], [[1, 3], [2, 4]],
                         [[4, 1], [3, 2]]]

    def round(self, detail):
        """
        gera ronda de grupo
        :return:
        """
        if detail:
            print('\n%s' % self.name)
        for teamidx in self.fixtures[self.matchday - 1]:
            match = Match(self._teams[teamidx[0] - 1], self._teams[teamidx[1] - 1])
            if user.team == match.hometeam or user.team == match.awayteam:
                match.key_match(et=False)
            else:
                match.complete_match(et=False, detail=detail)

            self._teams[teamidx[0] - 1].add_groupgoals(match.homegoals, match.awaygoals)
            self._teams[teamidx[1] - 1].add_groupgoals(match.awaygoals, match.homegoals)

        self.matchday += 1

    def generate_table(self):
        """
        Gera tabela de pontos do grupo
        :return:
        """
        data = list()
        for team in self._teams:
            data.append([team, team.groupstats["matches"], team.groupstats["wins"], team.groupstats["draws"],
                         team.groupstats["losses"], team.groupstats["goals_scored"], team.groupstats["goals_conceded"],
                         team.groupstats["points"]])

        table = pd.DataFrame(data, columns=['Team', 'Played', 'Wins', 'Draws', 'Losses', 'Goals For', 'Goals Against',
                                            'Points'])
        table = table.sort_values(by=['Points', 'Goals For', 'Goals Against'], ascending=[False, False, True])

        table = table.assign(Position=[1 + i for i in range(len(table))])[['Position'] + table.columns.tolist()]
        table = table.set_index('Position')
        table_str = table.to_string()
        return table, table_str








