#! /usr/bin/env python
# -*- coding: utf-8 -*-

import random


class Training:

    def __init__(self):
        pass

    def random_attribute_change(self, team):
        """
        selecciona um atributo para aumentar
        utiliza todos os pontos de treino num só atributo de um jogador
        """
        player = team.random_player()
        atlist = player.atlist
        name = random.choice(atlist)
        value_change = player.change_attribute(attr_name=name, value=team.trainingpoints,
                                               trainingpoints=team.trainingpoints)
        team.update_trainingpoints(value_change)

    def train(self, user, idx, attr, value):
        value_change = user.team._players[idx].change_attribute(attr_name=attr, value=value,
                                                                trainingpoints=user.team.trainingpoints)
        user.team.update_trainingpoints(value_change)
