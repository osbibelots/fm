#! /usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
from match_class import *
from random import shuffle


class League:
    def __init__(self, name, country, teams=None):
        self.name = name
        self.country = country
        if teams is not None:
            self._teams = list(teams)
        else:
            self._teams = []
        self.season = 1
        self.matchday = 1
        self.schedule = None
        self.matchweek = list()
        self.winners = list()
        self.prizes = list()
        self.setprizes()

    def __len__(self):
        """
        retorna o número de jornadas
        :return:
        """
        return (len(self._teams) - 1) * 2

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __call__(self):
        """
        Se chamares a liga, ela retorna a tabela de pontuações
        :return: tabela de pontos
        """
        _, table = self.generate_table()
        return '\nSeason ' + str(self.season) + '\n' + table

    def __getitem__(self, item):
        """
        itera sobre a tabela, liga[0] vai dar o primeiro classificado
        :param item:
        :return:
        """
        table, _ = self.generate_table()
        return table.iloc[item, 0]

    def setprizes(self):
        """
        define premios de fim de epoca consante posicao na liga, ate agora só estao os valores da premier league
        :return:
        """
        self.prizes = [38, 36, 34, 32, 30, 28, 27, 25, 23, 21, 19, 17, 15, 13, 11, 9.5, 7.5, 5.5, 4, 2]

    def giveprizes(self):
        """
        dá premio monetario no final da epoca consoante a posicao na liga
        :return:
        """
        for idx, i in enumerate(self):
            i.budget += self.prizes[idx]

    def generate_schedule(self):
        """
        gera um array com as semanas em que se joga na liga, a partir da semana 32
        :return:
        """
        self.matchweek = list()
        i = 32
        if len(self._teams) == 18:
            skip = [35, 41, 42, 51, 52, 12]
        if len(self._teams) == 20:
            skip = [42, 12]
        while len(self.matchweek) < len(self):
            if i in skip:
                pass
            else:
                self.matchweek.append(i)
            i += 1
            if i == 53:
                i = 1

    def add_team(self, team):
        self._teams.append(team)

    def add_matchday(self):
        self.matchday += 1

    def increment_season(self):
        """
        acresce uma época e reinicializa todos os atributos das equipas/jogadores
        :return:
        """
        self.setwinners()
        self.giveprizes()
        self.season += 1
        self.matchday = 1
        shuffle(self._teams)
        self.initialize_teams()

    def printwinnertable(self):

        table = pd.DataFrame(self.winners, columns=['Season', '1st', '2nd', '3rd'])
        table = table.sort_values(by=['Season'], ascending=[True])
        table = table.set_index('Season')

        table_str = table.to_string()
        if table.empty:
            print('There are no winners yet!')
        else:
            print('\n%s' % self.name)
            print(table_str)

    def setwinners(self):
        winner, runnerup, third = self[0], self[1], self[2]
        row = [self.season, winner, runnerup, third]
        self.winners.append(row)

    def generate_matchday(self, detail):
        """
        Simula uma jornada para a liga
        :param fixture: tem de ser um array de listas de pares
        :return:
        """
        usermatch = None
        if self.matchday == 1:
            self.schedule = self.generate_fixtures()
        if detail:
            print("\n%s\nMatchday %d:" % (self.name, self.matchday))
        for i in range(0, len(self._teams) // 2):
            hometeam, awayteam = self._teams[self.schedule[self.matchday - 1][i][0] - 1], \
                                 self._teams[self.schedule[self.matchday - 1][i][1] - 1]

            if hometeam == user.team or awayteam == user.team:
                usermatch = Match(hometeam, awayteam)
            else:
                match = Match(hometeam, awayteam)
                match.complete_match(et=False)
                homegoals, awaygoals = match.homegoals, match.awaygoals
                hometeam.add_goals(homegoals, awaygoals)
                awayteam.add_goals(awaygoals, homegoals)
                if detail:
                    print("%s %s - %s %s" % (hometeam, homegoals, awaygoals, awayteam))

        if usermatch is not None:
            usermatch.key_match(et=False)
            homegoals, awaygoals = usermatch.homegoals, usermatch.awaygoals
            usermatch.hometeam.add_goals(homegoals, awaygoals)
            usermatch.awayteam.add_goals(awaygoals, homegoals)
            if detail:
                print("%s %s - %s %s" % (usermatch.hometeam, homegoals, awaygoals, usermatch.awayteam))

        self.add_matchday()
        if self.matchday > len(self):
            self[0].moralechange(20)
            if detail:
                print("\n%s wins the %s!!!\n" % (self[0], self.name))

    def generate_season(self):
        """
        Simula uma temporada inteira iterando sobre as jornadas
        :return: nada, apenas adiciona atributos às equipas
        """
        schedule = self.generate_fixtures()

        for day in schedule:
            self.generate_matchday(day)

        self.increment_season()

    def initialize_teams(self):
        """
        limpa atributos no fim da época tipicamente
        :return:
        """
        for team in self._teams:
            team.cleanseason()

    def generate_table(self):
        """
        Gera tabela de pontos da liga
        :return:
        """
        data = list()
        for team in self._teams:
            data.append([team, team.matches, team.wins, team.draws, team.losses, team.goals_scored, team.goals_conceded,
                         team.points])

        table = pd.DataFrame(data, columns=['Team', 'Played', 'Wins', 'Draws', 'Losses', 'Goals For', 'Goals Against',
                                            'Points'])
        table = table.sort_values(by=['Points', 'Goals For', 'Goals Against'], ascending=[False, False, True])

        # atribuir posicao da equipa depois de ordenada a tabela
        table = table.assign(Position=[1 + i for i in range(len(table))])[['Position'] + table.columns.tolist()]
        table = table.set_index('Position')
        table_str = table.to_string()
        return table, table_str

    def league_scorers(self):
        """
        Junta todos os marcadores numa só tabela e organiza por numero de golos
        :return: devolve tabela de melhores marcadores da liga
        """
        data = list()
        for team in self._teams:
            # _ ignora o primeiro argumento que não nos interessa
            _, teamtable = team.scorer_list()
            data.append(teamtable)

        table = pd.concat(data)

        table = table.sort_values(by=['Goals'], ascending=[False])

        table = table.assign(Top=[1 + i for i in range(len(table))])[['Top'] + table.columns.tolist()]
        table = table.set_index('Top')
        table = table.head(10)
        table = table.to_string()

        return table

    def generate_fixtures(self):
        """
        Gera a lista de jornadas da liga
        :return: um array com números, cada número corresponderá
        ao index da equipa no array equipas
        """
        def make_day(num_teams, day):
            # using circle algorithm, https://en.wikipedia.org/wiki/Round-robin_tournament#Scheduling_algorithm
            assert not num_teams % 2, "Number of teams must be even!"
            # generate list of teams
            lst = list(range(1, num_teams + 1))
            # rotate
            day %= (num_teams - 1)  # clip to 0 .. num_teams - 2
            if day:  # if day == 0, no rotation is needed (and using -0 as list index will cause problems)
                lst = lst[:1] + lst[-day:] + lst[1:-day]
            # pair off - zip the first half against the second half reversed
            half = num_teams // 2
            return list(zip(lst[:half], lst[half:][::-1]))

        def make_schedule(num_teams):
            """
            Produce a double round-robin schedule
            """
            # number of teams must be even
            if num_teams % 2:
                num_teams += 1  # add a dummy team for padding

            # build first round-robin
            schedule = [make_day(num_teams, day) for day in range(num_teams - 1)]
            # generate second round-robin by swapping home,away teams
            swapped = [[(away, home) for home, away in day] for day in schedule]

            return schedule, swapped

        def get_finalschedule():
            first, second = make_schedule(len(self._teams))
            initial_round = list()
            for i in range(0, (len(self._teams) - 1)):
                if i % 2 == 0:
                    initial_round.append(first[i])
                else:
                    initial_round.append(second[i])

            swapped = [[(away, home) for home, away in day] for day in initial_round]

            schedule = initial_round + swapped

            return schedule

        schedule = get_finalschedule()
        return schedule



