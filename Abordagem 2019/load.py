#! /usr/bin/env python
# -*- coding: utf-8 -*-

import xlrd
import os
from player_class import *
from team_class import *
from league_class import *
from cup_class import *
from country_class import *
from training_class import *


class Loader:
    def __init__(self):
        self.filepath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Players.xlsx')
        self.countrylist = list()
        self.playerlist = list()
        self.teamlist = list()
        self.leaguelist = list()
        self.cuplist = list()
        self.europeancuplist = list()

    def loadcountries(self):
        xl_workbook = xlrd.open_workbook(self.filepath)
        xl_sheet = xl_workbook.sheet_by_index(2)

        for idx in range(1, xl_sheet.nrows):
            row = xl_sheet.row_slice(idx)
            self.countrylist.append(Country(name=row[1].value))

    def loadplayers(self):
        """
        cria todos os jogadores
        :return:
        """
        # Vai buscar o book e a sheet onde estao os jogadores
        xl_workbook = xlrd.open_workbook(self.filepath)
        xl_sheet = xl_workbook.sheet_by_index(0)

        # itera sobre as linhas e vai criando instances
        for idx in range(1, xl_sheet.nrows):
            row = xl_sheet.row_slice(idx)
            self.playerlist.append(Player(name=row[0].value, team=row[1].value, position=row[2].value, teamplay=int(row[3].value),
                                attack=int(row[4].value), defence=int(row[5].value), stamina=int(row[6].value),
                                finishing=int(row[7].value), penaltytaking=int(row[9].value), goalkeeping=int(row[8].value),
                                age=int(row[10].value)))

    def loadteams(self):
        """
        cria todas as equipas
        :return:
        """
        # Vai buscar o book e a sheet onde estao os jogadores
        xl_workbook = xlrd.open_workbook(self.filepath)
        xl_sheet = xl_workbook.sheet_by_index(1)

        # itera sobre as linhas e vai criando instances
        for idx in range(1, xl_sheet.nrows):
            row = xl_sheet.row_slice(idx)
            self.teamlist.append(Team(name=row[0].value, league=row[1].value, country=row[2].value))

    def loadleagues(self):
        """
        cria todas as ligas
        :return:
        """
        # Vai buscar o book e a sheet onde estao os jogadores
        xl_workbook = xlrd.open_workbook(self.filepath)
        xl_sheet = xl_workbook.sheet_by_index(2)

        # itera sobre as linhas e vai criando instances
        for idx in range(1, xl_sheet.nrows):
            row = xl_sheet.row_slice(idx)
            self.leaguelist.append(League(name=row[0].value, country=row[1].value))

    def loadcups(self):
        """
        cria todas as taças
        :return:
        """
        # Vai buscar o book e a sheet onde estao os jogadores
        xl_workbook = xlrd.open_workbook(self.filepath)
        xl_sheet = xl_workbook.sheet_by_index(3)

        # itera sobre as linhas e vai criando instances
        for idx in range(1, xl_sheet.nrows):
            row = xl_sheet.row_slice(idx)
            self.cuplist.append(Cup(name=row[0].value, country=row[1].value))

    def loadeuropeancups(self):
        # Vai buscar o book e a sheet onde estao os jogadores
        xl_workbook = xlrd.open_workbook(self.filepath)
        xl_sheet = xl_workbook.sheet_by_index(4)

        # itera sobre as linhas e vai criando instances
        for idx in range(1, xl_sheet.nrows):
            row = xl_sheet.row_slice(idx)
            self.europeancuplist.append(EuropeanCup(name=row[0].value))

    def setcountryleague(self):
        self.loadcountries()
        for country in self.countrylist:
            for league in self.leaguelist:
                if league.country == country.name:
                    country.add_league(league)

    def setsquads(self):
        """
        coloca os jogadores nas respectivas equipas
        :return:
        """
        self.loadplayers()
        self.loadteams()
        for player in self.playerlist:
            for team in self.teamlist:
                if player.team == team.name:
                    team.add_player(player)
                    break

        for team in self.teamlist:
            team.setattack()
            team.setdefence()
            team.setfatigue()

    def setleagueteams(self):
        """
        preenche as ligas com as respectivas equipas
        :return:
        """
        self.loadleagues()
        for team in self.teamlist:
            for league in self.leaguelist:
                if team.league == league.name:
                    league.add_team(team)
                    # depois de adicionar a equipa à liga, redifine a liga da equipa com o proprio objecto liga
                    # para nao ser string do excel

    def initializecupteams(self):
        self.resetcupteams()
        self.setcupteams()

    def resetcupteams(self):
        for cup in self.cuplist:
            cup.cleanteams()

    def setcupteams(self):
        """
        preenche as taças com as respectivas equipas
        isto tem de ser chamado no final de cada época porque nas tacas vao sendo removidas equipas
        :return:
        """
        for team in self.teamlist:
            for cup in self.cuplist:
                    if team.country == cup.country:
                        cup.add_team(team)

    def setcompetitions(self):
        """
        chama os metodos de preencher planteis e competiçoes
        :return:
        """
        self.loadcups()
        self.setsquads()
        self.setleagueteams()
        self.setcupteams()

    def redefine_as_obj(self):
        """
        redefinir tudo o que vem de string do excel para um objecto mesmo
        (exemplo: nas equipas as ligas vêm como string, aqui reconverte para objecto)
        :return:
        """
        for team in self.teamlist:
            for league in self.leaguelist:
                if team.league == league.name:
                    team.league = league
                for cup in self.cuplist:
                    if team.cup == cup.name:
                        team.cup = cup
                    for country in self.countrylist:
                        if team.country == country.name:
                            team.country = country

        for league in self.leaguelist:
            for country in self.countrylist:
                if league.country == country.name:
                    league.country = country
                    country.league = league

        for cup in self.cuplist:
            for country in self.countrylist:
                if cup.country == country.name:
                    cup.country = country
                    country.cup = cup


load = Loader()
load.setcountryleague()
load.setcompetitions()
load.redefine_as_obj()
load.loadeuropeancups()

training = Training()

for league in load.leaguelist:
    shuffle(league._teams)







