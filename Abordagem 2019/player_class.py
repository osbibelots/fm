#! /usr/bin/env python
# -*- coding: utf-8 -*-


class Player:
    """Classe que cria jogadores com certos atributos e posição no campo"""
    def __init__(self, name, team, attack, defence, teamplay, stamina, finishing, position, penaltytaking, age,
                 goalkeeping=None):
        self.name = name
        self.team = team
        self.age = age
        self.attack = attack
        self.defence = defence
        self.teamplay = teamplay
        self.stamina = stamina
        self.finishing = finishing
        self.position = position
        if goalkeeping is not None:
            self.goalkeeping = goalkeeping
        if self.position == 'GK':
            self.atlist = ['attack', 'defence', 'teamplay', 'stamina', 'finishing', 'goalkeeping', 'penaltytaking']
        else:
            self.atlist = ['attack', 'defence', 'teamplay', 'stamina', 'finishing', 'penaltytaking']
        self.goals = 0
        self.matches = 0
        self.penaltytaking = penaltytaking
        self.fatigue = 0
        self.matchattributes = {"Attack": self.attack, "Defence": self.defence,
                                "Teamplay": self.teamplay, "Stamina": self.stamina, "Finishing": self.finishing}
        self.matchattributesgk = {"Goalkeeping": self.attack, "Defence": self.defence,
                                "Teamplay": self.teamplay, "Stamina": self.stamina, "Finishing": self.finishing}

    def __str__(self):
        return self.name

    def __repr__(self):
        """
        representaçao do objecto é o nome do jogador
        """
        return self.name

    def __eq__(self, other):
        """
        compara equipas, se não for instance dá logo False,
        se tiver outro nome, dá false também
        :param other:
        :return:
        """
        if isinstance(other, Player):
            return self.name == other.name
        return False

    def update_attributes(self):
        self.matchattributes = {"Attack": self.attack, "Defence": self.defence,
                                "Teamplay": self.teamplay, "Stamina": self.stamina, "Finishing": self.finishing}
        self.matchattributesgk = {"Goalkeeping": self.attack, "Defence": self.defence,
                                  "Teamplay": self.teamplay, "Stamina": self.stamina, "Finishing": self.finishing}

    def cleanseason(self):
        self.goals = 0
        self.matches = 0

    def add_goal(self):
        self.goals += 1

    def add_match(self):
        self.matches += 1

    def add_age(self, value):
        self.age += value

    def attribute_development(self):
        """ colocar aqui uma logica de queda de atributos"""
        if self.age > 30:
            self.stamina -= 3

    def add_fatigue(self, value):
        """
        fadiga acumulada de jogada para jogada
        :param value:
        :return:
        """
        self.fatigue += value * (50 / self.stamina)
        if self.fatigue > 100:
            self.fatigue = 100

    def remove_fatigue(self, value, days):
        """
        recuperaçao de fadiga de dia para dia
        :param value:
        :return:
        """
        self.fatigue -= value * days
        if self.fatigue < 0:
            self.fatigue = 0

    def change_attribute(self, attr_name, value, trainingpoints):
        """
        Altera atributos
        :param string: Nome do atributo
        :param value: valor a acrescentar ao atributo
        """
        if hasattr(self, attr_name):
            if getattr(self, attr_name) >= 99:
                print('%s %s has already reached his maximum.' % (self.name, attr_name))
                return 0
            else:
                if value > trainingpoints:
                    print('Not enough training points')
                    return 0
                else:
                    setattr(self, attr_name, getattr(self, attr_name) + value)
                    self.update_attributes()
                    print('%s %s rose to %s' % (self.name, attr_name, getattr(self, attr_name)))
                    return -value



