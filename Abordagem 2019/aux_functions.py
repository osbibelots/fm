#! /usr/bin/env python
# -*- coding: utf-8 -*-


def cube(x):
    if x >= 0:
        return x**(1/3)
    elif x < 0:
        return -(abs(x)**(1/3))


def power2(x):
    powers = [2, 4, 8, 16, 32, 64, 128, 256, 512]
    for idx, num in enumerate(powers):
        if num > x:
            return num, idx
        else:
            pass
