#! /usr/bin/env python
# -*- coding: utf-8 -*-


class User:
    """
    team, country, league, lastmatch sao objectos
    """
    def __init__(self):
        self.team = ''
        self.country = ''
        self.cup = None
        self.league = None
        self.lastmatch = None
        self.matchweek = list()
        self.europeancup = None
        self.fixtures = list()

    def set_team(self, team):
        """define a equipa do utilizador"""
        self.team = team

    def set_country(self, country):
        """
        o utilizador pode ter apenas um país, pode não ter equipa
        por isso aqui é logo definida a liga que vai ser vista em detalhe
        :param country:
        :return:
        """
        self.country = country
        self.league = country.league
        self.cup = country.cup
        self.generatecountryschedule()

    def seteuropeancup(self, europeancup):
        self.europeancup = europeancup

    def savelastmatch(self, value):
        """
        :param value:  é uma instance da classe Match
        :return:
        """
        self.lastmatch = value
        self.fixtures.append(value)

    def generatecountryschedule(self):
        for date in self.league.matchweek:
            self.matchweek.append(date)
        for date in self.cup.matchweek:
            self.matchweek.append(date)
        if self.europeancup is not None:
            for date in self.europeancup.matchweek:
                self.matchweek.append(date)

    def getleagueposition(self):
        position = 0
        for idx, team in enumerate(user.league):
            if user.team == team:
                position = idx + 1
                break
        return position


user = User()
