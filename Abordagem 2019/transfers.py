#! /usr/bin/env python
# -*- coding: utf-8 -*-

from load_teams import *


class Transfer:
    def transfer_player(self, teamOut, player, teamIn):
        teamIn.add_player(player)
        teamOut.remove_player(player)


transfer = Transfer()
benfica.list_players()
porto.list_players()
transfer.transfer_player(benfica, Grimaldo, porto)
benfica.list_players()
porto.list_players()

