#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from Calendar import *
from load import *


class Menu:
    def __init__(self):
        self.initial = False
        self.winner = False

    def initializeschedules(self):
        """
        inicializa os calendarios das semanas onde vao ser os jogos
        ligas aos domingos, taças de país as quintas, taças euro as terças
        :return:
        """
        load.initializecupteams()

        for league in load.leaguelist:
            league.generate_schedule()
            league.matchweek = calendar.setschedule(league.matchweek, 0)
        for cup in load.cuplist:
            cup.generate_schedule()
            cup.matchweek = calendar.setschedule(cup.matchweek, 4)
        if calendar.season > 1:
            for europeancup in load.europeancuplist:
                europeancup.generate_schedule()
                europeancup.matchweek = calendar.setschedule(europeancup.matchweek, 2)

    def initialize(self):
        self.initializeschedules()
        self.choose_country()
        self.choose_team()
        self.initialmenu()

    def choose_country(self):
        for idx, country in enumerate(load.countrylist):
            print('%s: %s' % (idx + 1, country))

        user_country = int(input('Country: ')) - 1
        country = load.countrylist[user_country]
        user.set_country(country)

    def choose_team(self):
        print('\n0: No Team')
        for league in load.leaguelist:
            if league.country == user.country:
                for idx, team in enumerate(league._teams):
                    print('%s: %s' % (idx + 1, team))
                break
        user_team = int(input('Team: ')) - 1

        if user_team == -1:
            user.team = ''
        else:
            user.set_team(league._teams[user_team])

    def initialmenu(self):
        self.initial = True
        while self.initial:
            print("\n\tNext Week: a\n\tTraining: t\n\tLeague: l \n\t"
                  "Information: i\n\tOther: o\n\tExit: s\n")

            text = input('Input: ')
            self.initialoption(text, calendar)

    def informationoption(self):
        print("\n\tTeam information: a\n\tPlayers attributes: b\n\tLast match: c"
              "\n\tFixtures: d\n\tExit to main menu: s\n")
        text = input('Input: ')
        if text == 'a':
            print('\nAttack: %s\nDefence: %s\nMorale: %s\nFatigue: %s\nMoney: %s' % (user.team.attack,
                                                                                     user.team.defence,
                                                                                   user.team.morale, user.team.fatigue,
                                                                                   user.team.budget))
        elif text == 'b':
            user.team.print_squadlist()
        elif text == 'c':
            if user.lastmatch is None:
                print('No games were played')
            else:
                user.lastmatch.generate_table()
        elif text == 's':
            self.initialmenu()
        else:
            print('Invalid entry|')

    def leaguemenu(self):
        print("\n\tLeague Table: a\n\tGoal Scorers: b\n\tExit to main menu: s\n")
        text = input('Input: ')
        if text == 'a':
            print(user.league())
        elif text == 'b':
            print(user.league.league_scorers())
        elif text == 's':
            self.initialmenu()
        else:
            print('Invalid entry|')

    def othermenu(self):
        print("\n\tOther league tables: a\n\tGoal Scorers: b\n\tCompetition winners: c\n\tExit to main menu: s\n")
        text = input('Input: ')
        if text == 'a':
            self.leaguetablesmenu()
        elif text == 'b':
            print(user.league.league_scorers())
        elif text == 'c':
            self.initial, self.winner = False, True
            self.winnermenu()
        elif text == 's':
            self.initialmenu()
        else:
            print('Invalid entry|')

    def initialoption(self, text, week):
        if text == 'i':
            self.informationoption()

        elif text == 'l':
            self.leaguemenu()

        elif text == 'o':
            self.othermenu()

        elif text == 't':
            self.trainingmenu()

        elif text == 'lol':
            user.league.printwinnertable()
        elif text == 'lol1':
            user.cup.printwinnertable()
        elif text == 'a':
            # Avança uma semana e verifica se ha jogos liga ou taça nessa semana
            week.add_week()
            self.simulation()

        elif text == 'f':
            while week.week != 30:  # ligaPT.matchday <= len(ligaPT):
                week.add_week()
                self.simulation()

        elif text == 'c':
            user.europeancup()
        elif text == 's':
            sys.exit()
        else:
            print('Invalid entry!!')

    def simulation(self):
        """
        simula ligas e taças, dando detalhe ao país do user, com restantes a correr em background
        :return:
        """
        leaguematch, cupmatch, europeancupmatch = False, False, False
        print('Week %i (%s/%s/%s)' % (calendar.week, calendar.day.day, calendar.day.month, calendar.day.year))
        for league in load.leaguelist:
            """simula ligas"""
            if calendar.day in league.matchweek:
                if league.country == user.country:
                    leaguematch = True
                    league.generate_matchday(detail=True)
                else:
                    league.generate_matchday(detail=False)
        for cup in load.cuplist:
            """simula tacas"""
            if calendar.day in cup.matchweek:
                if cup.country == user.country:
                    cupmatch = True
                    cup.generate_knockoutround(detail=True)
                else:
                    cup.generate_knockoutround(detail=False)

        for europeancup in load.europeancuplist:
            if europeancup.matchweek is not None:
                if calendar.day in europeancup.matchweek:
                    europeancupmatch = True
                    europeancup.generate_matchday(detail=True)

        if not leaguematch and not cupmatch and not europeancupmatch:
            print('No matches to play this week.')

        if calendar.week == 24:
            calendar.increment_season()
            position = user.getleagueposition()
            # champions
            if calendar.season > 1:
                for league in load.leaguelist:
                    for k in range(0, 16):
                        load.europeancuplist[0].add_team(league[k])
                if user.team == '':
                    user.seteuropeancup(load.europeancuplist[0])
                load.europeancuplist[0].generategroups()
                if 0 < position <= 16:
                    user.seteuropeancup(load.europeancuplist[0])

            for league in load.leaguelist:
                league.increment_season()
            for cup in load.cuplist:
                cup.increment_season()

            self.initializeschedules()
            user.generatecountryschedule()

    def winnermenu(self):
        """
        menu para consultar vencedores de competiçoes
        :return:
        """
        while self.winner:
            print('\n\n0: Previous menu')
            leagueidx, cupidx = 0, 0
            for idx, item in enumerate(load.leaguelist):
                print('%s: %s' % (idx + 1, item.name))
                leagueidx = idx + 1
            for idx, item in enumerate(load.cuplist):
                print('%s: %s' % (idx + 1 + leagueidx, item.name))
                cupidx = idx + 1
            for idx, item in enumerate(load.europeancuplist):
                print('%s: %s' % (idx + 1 + leagueidx + cupidx, item.name))

            text = int(input('Input: '))
            if text == 0:
                self.initialmenu()
            elif text > cupidx + leagueidx:
                text = text - (cupidx + leagueidx)
                load.europeancuplist[text - 1].printwinnertable()
            elif text > leagueidx:
                text = text - leagueidx
                load.cuplist[text - 1].printwinnertable()
            elif text <= leagueidx:
                load.leaguelist[text - 1].printwinnertable()
            else:
                print('Invalid entry!')

    def leaguetablesmenu(self):
        for idx, item in enumerate(load.leaguelist):
            print('%s: %s' % (idx, item.name))

        text = int(input('Input: '))
        print(load.leaguelist[text]())

    def playerstraining(self, text):
        """mostra atributos do jogador escolhido"""
        keylist = list()
        if user.team._players[text].position == 'GK':
            for key, value in user.team._players[text].matchattributesgk.items():
                print('%s %s' % (key, value))
                keylist.append(key.lower())
        else:
            for key, value in user.team._players[text].matchattributes.items():
                print('%s %s' % (key, value))
                keylist.append(key.lower())
        return keylist

    def trainingmenu(self):
        """Lista de jogadores"""
        print('Available training points: %s' % user.team.trainingpoints)
        print('\n\n0: Previous menu')
        for idx, item in enumerate(user.team._players):
            print('%s: %s' % (idx + 1, item.name))

        try:
            text = int(input('\nInput: ')) - 1
        except ValueError:
            print('Invalid entry')
            self.trainingmenu()

        if text == - 1:
            self.initialmenu()
        elif text >= len(user.team):
            print('Invalid entry')
            self.trainingmenu()
        else:
            keylist = self.playerstraining(text)

            text_attribute = input('\nInput (name of the attribute and the number of points after space, 0 to go back): ')
            if text_attribute == '0':
                self.trainingmenu()
            else:
                if " " not in text_attribute:
                    print('Name of attribute and number of points required')
                    self.trainingmenu()
                else:
                    text_input = text_attribute.split(" ")
                    # espera o nome do atributo
                    # e o valor dos pontos a submeter, com espaço no meio
                    text_attribute, text_value = text_input[0], int(text_input[1])
                    text_attribute = text_attribute.lower()
                    if text_attribute not in keylist:
                        print('Invalid entry')
                        self.trainingmenu()
                    else:
                        training.train(user, text, text_attribute, text_value)

                        self.trainingmenu()


menu = Menu()











